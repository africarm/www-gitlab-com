#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/redirect'

exact_err = <<~EOS
if ( obj.status == 703 ) {
  set obj.status = 301;
  set obj.response = "Moved Permanently";
  set obj.http.Location = req.http.redir_location;
  synthetic {""};
  return (deliver);
}
EOS

exact_recv = <<~EOS
set req.http.redir_location = table.lookup(redirects_match, req.url, "");

if ( req.http.redir_location != "" ) {
  if ( req.http.redir_location !~ "^http" ) {
    set req.http.redir_location = "https://" req.http.host req.http.redir_location;
  }
  error 703 "Permanent Redirect";
}
EOS

exact_err_path = "/snippet/#{ENV['FASTLY_EXACT_ERR_SNIPPET_ID']}"
exact_recv_path = "/snippet/#{ENV['FASTLY_EXACT_RECV_SNIPPET_ID']}"
exact_err_data = "content=" + exact_err
exact_recv_data = "content=" + exact_recv
dictionary_id = ENV['FASTLY_DICT_ID']
headers_dict = { 'Fastly-Key' => ENV['FASTLY_API_KEY'], 'Content-Type' => 'application/json' }

Gitlab::Homepage::Redirect.fastly_snippet_patch(exact_err_path, exact_err_data)
Gitlab::Homepage::Redirect.fastly_snippet_patch(exact_recv_path, exact_recv_data)

resp = Gitlab::Homepage::FastlyClient.get("/dictionary/#{dictionary_id}/items", headers: headers_dict)
current_redirects = resp.parsed_response
items = []
Gitlab::Homepage::Redirect.from_definitions_file do |redirect|
  next if redirect.comp_op != '='

  target = redirect.target

  redirect.sources.each do |source|
    # Delete still existing redirects from the to-be-deleted list
    current_redirects.delete_if { |r| r['item_key'] == source }

    items << { op: 'upsert', item_key: source, item_value: target }
  end
end

Gitlab::Homepage::FastlyClient.patch("/dictionary/#{dictionary_id}/items", body: { items: items }.to_json, headers: headers_dict)

# Delete items still remaining, that means they were deleted from redirects.yml
current_redirects.each do |redirect|
  key = redirect['item_key'].gsub('/', '%2F')
  Gitlab::Homepage::FastlyClient.delete("/dictionary/#{dictionary_id}/item/#{key}", headers: headers_dict)
end
