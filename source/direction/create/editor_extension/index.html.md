---
layout: markdown_page
title: "Category Direction - Editor Extension"
---

- TOC
{:toc}

## Editor Extension

| | |
| --- | --- |
| Stage | [Create](/direction/dev/#create) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2020-06-24` |

### Introduction and how you can help
Thanks for visiting this direction page on Editor Extension. This page belongs to the [Editor](/handbook/product/product-categories/#editor-group) group of the Create stage and is maintained by Kai Armstrong ([E-Mail](mailto:karmstrong@gitlab.com)).

This direction is constantly evolving and everyone can contribute:

 - Please comment and contribute in the linked issues and epics on this page. Sharing your feedback directly on GitLab.com  or submitting a Merge Request to this page are the best ways to contribute to our strategy.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for Local Development Tools, we'd especially love to hear from you.

### Overview

GitLab supports teams collaborating and building software together, however that collaboration is only available inside GitLab application. 

Developers, on the other hand, spend the majority of their time working in local editors implementing work outlined in issues, responding to merge request feedback and testing/debugging their applications. These tasks are the core of the developer experience, but GitLab is missing from this experience in any integrated way.

### Target Audience and Experience
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category. An overview
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels. -->

#### Challenges to address
<!--
- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->

### Where we are headed

GitLab should support developers closer to where they're doing their meaningful work and enable them to be more efficient in the delivery of that work.

### Maturity

### Problems to Solve

There are many specific problems that can be solved by focusing on local developer tooling:

1. [Developers working in local editors](https://gitlab.com/groups/gitlab-org/-/epics/2431)
1. Developers working locally with local runtime
1. [Developers working locally with remote runtime ](https://gitlab.com/groups/gitlab-org/-/epics/3230)

### What's Next & Why

**In progress:** VS Code Extension Housekeeping [&3350](https://gitlab.com/groups/gitlab-org/-/epics/3350)

GitLab has recently adopted a [VS Code Extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension). In order to make the project ready for contributing and future development some general housekeeping needs to be done.

**Next:** Remote Development Environments [&3230](https://gitlab.com/groups/gitlab-org/-/epics/3230)

Developing a strategy and MVC for enabling developers to test and debug their applications utilizing remote compute will be key in expanding in this path.

### What is Not Planned Right Now

We're not currently focused on extensions for any other local editors or IDEs. We recognize there are a variety of these environments and we'll continue to monitor demand and market trends to look for other opportunities to support developers.

The editor group is also not looking to connect our existing [Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/index.html) to any runtime environments or local tools for further development. We may continue to explore a more advanced Web IDE that could support these itmes in the future.

### Competitive Landscape

#### Local Editors

 - [GitHub Pull Requests and Issues](https://marketplace.visualstudio.com/items?itemName=GitHub.vscode-pull-request-github)

#### Local Runtime

 - [Skaffold](https://skaffold.dev/)

#### Remote Runtime

 - [Okteto](https://okteto.com)
 - [Tilt](https://tilt.dev)
 - [Dev Pods](https://jenkins-x.io/docs/reference/devpods/)

<!-- ### Analyst Landscape -->


<!-- ### Top Customer Success/Sales issue(s) -->
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->


<!-- ### Top user issue(s) -->
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->


<!-- ### Top internal customer issue(s) -->
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->


<!-- ### Top Vision Item(s) -->
<!-- What's the most important thing to move your vision forward?-->

