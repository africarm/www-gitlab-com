---
layout: markdown_page
title: "Category Direction - Subgroups"
description: "Groups are a fundamental building block (a small primitive) in GitLab for project organization and managing access to these resources at scale. Learn more!"
canonical_path: "/direction/manage/subgroups/"
---

- TOC
{:toc}

| **Stage** | **Maturity** | **Content Last Reviewed** |
| --- | --- | --- |
| [Manage](/direction/manage/) | [Complete](/direction/maturity/) | `2020-06-05` |

## Introduction and how you can help
Thanks for visiting this category page on Subgroups in GitLab.  This page is being actively maintained by [Melissa Ushakov](https://about.gitlab.com/company/team/#mushakov). This vision and direction is a work in progress and sharing your feedback directly on issues and epics on GitLab.com is the best way to contribute. 

## Overview

Groups are a fundamental building block (a [small primitive](https://about.gitlab.com/handbook/product/product-principles/#prefer-small-primitives)) in GitLab that serve to:  
- Define workspace boundaries, particularly on GitLab.com, where a top level group frequently represents an organization
- Group users to manage authorization at scale
- Organize related projects together
- House features that cover multiple projects like epics, contribution analytics, and the security dashboard

### Our mission

In 2020, our goal is to make improvements by extending Groups to be able to help enterprise organizations thrive on GitLab.com. We will accomplish that by iterating on existing constructs and streamlining workflows to guide users to intended usage patterns.  

### Themes for 2020 

#### Challenges

Historically, enterprise customers have gravitated toward self-managed GitLab as their favored solution. With the proliferation of cloud services, enterprises are looking for options to use GitLab with managed infrastructure. We need to provide a SaaS platform that can safely and securely house our customers while reducing the load and cost of having to manage and configure their own infrastructure.

We seek to solve a number of problems with the current SaaS experience:
* **Isolation** - Create clear workspace boundaries in GitLab.com. This will prevent users from unintentionally exposing users, projects, or other sensitive information to the rest of the GitLab.com instance.
* **Control** - Elevate group owners in GitLab.com to have more control over their workspace and its users. Since users on GitLab.com are attributed to the instance - not the group - group owners lack the ability to directly administer, manage, and assist users in the group, instead of working through GitLab Support. 

#### Where We're Headed 

In order to better accommodate enterprise customers, we will strengthen the layer around Groups, Projects, and the teams of people working in them. We need mechanisms to isolate an organization from the rest of GitLab.com so that we can meet enterprise regulatory and security requirements. 

![Workspace](source/images/direction/manage/workspace_small.png)

Today, we are able to meet basic requirements to represent an organization's constructs by using groups. However, it is not a seamless experience. We need to offer additional configuration options to optimize the experience for each of the functions that groups serve. Please note that in the future state, we have a collection of objects with workspaces housing all of them. They are not a necessarily a hierarchy. Each object can be a hierarchy or a standalone object. 

Eventually, we can use top-level groups to bring instance-level features to GitLab.com. We are moving towards bringing the experience on GitLab.com and a self-managed environment closer together. 


## Target audience and experience

Groups are used by all GitLab users, no matter what role. However, heavier users of Groups in terms of organization and management of projects and users consist of:

* **Group Owners**
* **System Admins**
* **Team Leaders, Directors, Managers**
* **Individual contributors** who predominantly work with GitLab's project management features

## What's Next and Why

### Define workspace boundaries

A top-level group should feel like a self-contained space where an enterprise can work. Users in a group owned by an enterprise should be able to complete all their day to day activities without leaving their organization's group. Group administrators should be able to do basic user management without needing to contact GitLab support. 

#### Step One: Forking rules
Today, the forking workflow in GitLab relies on a user’s personal namespace by default. This is troublesome to enterprises because it means their intellectual property has to leave their namespace and is out of their control. We will build functionality to enable a complete forking workflow within a namespace. This includes disabling forking outside a group and creation of a personal workspace inside a group.

#### Step Two: Workspace context awareness
In GitLab.com, a user has a personal namespace by default and can belong to groups. The context that a user is operating in isn't always clear and we often default actions to a user’s personal namespace. We will adjust defaults, and create more visual indicators for group context.

#### Step Three: Manage user details
On GitLab.com, user accounts and all associated details are owned by a user, not the groups they belong to. This is problematic for group administrators since they can't ensure the level of data integrity necessary for their audit and regulatory needs. We will introduce ways for users to set group-specific profile information that they can allow group owners to manage.  

#### Step Four: User support
We plan to give group administrators the ability to manage their users more effectively so they can decrease their need to contact GitLab support. Group users will be able to "opt-in" to have their group owners assist them. Group administrators will be elevated and be allowed to perform administrative actions like password resets, MFA resets, and impersonation. 

### Manage access at scale

#### Step One: Hierarchy inheritance
Enterprises on GitLab.com work in a single hierarchy with top-down inheritance. Features like SSO require all group members to be added as a member to the top-level group, effectively giving them some access to all subgroups. We need to offer options to limit access so we can meet enterprise use cases. We can achieve this by creating a way for users to have no access when initially created, optionally disabling inheritance, and allowing multiple hierarchies. 

#### Step Two: Optimize groups for authorization
Groups offer the ability to group users and then manage permission by sharing to another group or project. We need to improve the discoverability and usability of this functionality. We will also explore the option of creating a "lean" version of our existing group functionality where we hide most things and only leave behind functionality pertaining to permissions management. 

#### Step Three: Optimize groups to represent teams
Once we have solidified the use of groups as a way to group users we extend functionality further and use groups to represent teams. We can work with other stages to use groups where teams need to be represented in analytics, project management, and portfolio management.  

## Maturity

This category is currently **Complete**. The next step in our maturity plan is achieving a **Lovable** state. 
* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.
* Note that a `Complete` state does not mean a category is "finished" and is no longer a priority. Even categories that are considered `Lovable` require continued investment.
<!-- ## Top user issue(s) -->

## Top internal customer issue(s)

🚨 GitLab Support Engineering: [Isolation & Control Requirements](https://gitlab.com/groups/gitlab-org/-/epics/2444)

<!-- ## Top Vision Item(s)

TBD -->
