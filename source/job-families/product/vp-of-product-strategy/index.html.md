---
layout: job_family_page
title: "VP of Product Strategy"
---

The VP of Product Strategy reports to the CEO and focuses on long-range impact including research, strategic initiatives, product strategy, and corporate development.

## Responsibilities

### Research

Research includes exploring technical and product topics of high uncertainty that may not bear fruit for a year or more, if ever. These are typically topics that are harder for the product-facing engineering organization to address. While the main goal is to develop and provide insight that the product engineering org can build on, it may include shipping early iterations to production.

- Explore cutting-edge technologies, tools, and trends.
- Produce research prototypes and mockups in far-future areas.

### Strategic Initiatives

Strategic initiatives have significant organizational impact or revenue potential, often exceeding $10M ARR, but that may not bear fruit for a year or more. This includes significant sales and partner programs.

- Identify and execute on key programs.
- Champion technical, product, and process change such as moving to realtime, getting to continuous deployment for GitLab.com, and incorporating product discovery.

### Product Strategy

Product vision and strategy has a [timescale of longer than a year](/handbook/ceo/cadence/#strategy).

- Own the product vision and strategy.
- Communicate the product vision and strategy with text and visual means (direction pages, blog posts, mockups, clickable demos, presentations, etc.).
- Contribute to product and R&D planning.

### Corporate Development

Corporate Development at GitLab is all about acquisitions.

- Identify acquisition areas and targets.
- Execute acquisitions.
- Ensure successful post-merger integration.

### Represent the company

- Part of the face of the company with blog posts, speaking engagements, customer visits, etc., especially to convey our vision and strategy.

## Requirements

- Have a strong, articulate, and correct vision for the future of GitLab and current shortcomings.
- Technical background.
- Enterprise software experience.
- Developer tool or platform industry experience.
- SaaS and self-managed (on-premises) experience.
- You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).
- [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#s-group)
- Ability to use GitLab

## Links

- [Product Handbook](/handbook/product)
- [Engineering Workflow](/handbook/engineering/workflow)

## Performance Indicators

* [Acquisition velocity](/handbook/acquisitions/performance-indicators/#acquisition-velocity)
* [Acquisition success](/handbook/acquisitions/performance-indicators/#acquisition-success)
* [Qualified acquisition targets](/handbook/acquisitions/performance-indicators/#qualified-acquisition-targets)
* Interactive prototypes created
* Moonshots landed
