# frozen_string_literal: true

require 'middleman'

#
# This middleman extension is used to speed up builds on CI
#
# We effectively do this by leveraging our CI parallelism features and split the build into six
# distinct partitions:
#
# 1. `PROXY_RESOURCE`: We leverage the `proxy` feature of middleman to generate e.g. job pages,
#    direction pages and others
# 2. `IMAGES`: We have a lot of images. So we let middleman copy them.
# 3. `BLOG_POST_OLD`: All blog posts up to 2017
# 4. `BLOG_POST_NEW`: All blog posts since 2018
# 5. `RELEASE_BLOG`: Release blog posts
# 6. `HANDBOOK_ENGINEERING`: The engineering handbook
# 7. `HANDBOOK_OTHER`: Other handbooks
# 8. `ASSETS`: javascripts, stylesheets, icons
# 9. `COMPANY`: All company pages and team images
# 10. `ALL_OTHERS`: All pages and files which do not fit into the categories above
#
# If `CI_NODE_INDEX` and `CI_NODE_TOTAL` are not set, e.g. on development machines, or turning
# parallelism off, it will simply be a noop extension
class PartialBuild < Middleman::Extension
  PROXY_RESOURCE = "proxy resources"
  UNUSED_1 = "Unused, used to be IMAGES"
  BLOG_POST_OLD = "blog posts old (up to 2017)"
  BLOG_POST_NEW = "blog posts new (since 2018)"
  RELEASE_BLOG = "release blog"
  UNUSED_5 = "Unused, used to be HANDBOOK_ENGINEERING_MARKETING"
  UNUSED_6 = "Unused, used to be HANDBOOK_OTHER"
  UNUSED_7 = "Unused, used to be ASSETS"
  COMPANY = "Company pages and team images"
  ALL_OTHERS = "all other pages"

  # We must ensure that this extension runs last, so that
  # the filtering works correctly
  self.resource_list_manipulator_priority = 1000

  def_delegator :@app, :logger

  def initialize(app, options_hash = {}, &block)
    super
    @enabled = ENV['CI_NODE_INDEX'] && ENV['CI_NODE_TOTAL'] || ENV['CI_BUILD_PROXY_RESOURCE']

    if ENV['CI_BUILD_PROXY_RESOURCE']
      @partial = PROXY_RESOURCE
      return
    end

    return unless @enabled

    raise "#{self.class.name}: If you want to enable parallel builds, please use exactly 9 parallel jobs. (CI_NODE_TOTAL='#{ENV['CI_NODE_TOTAL']}')" unless ENV['CI_NODE_TOTAL'].to_i == 9

    @partial = case ENV['CI_NODE_INDEX']
               when "1"
                 UNUSED_1
               when "2"
                 BLOG_POST_OLD
               when "3"
                 BLOG_POST_NEW
               when "4"
                 RELEASE_BLOG
               when "5"
                 UNUSED_5
               when "6"
                 UNUSED_6
               when "7"
                 UNUSED_7
               when "8"
                 COMPANY
               when "9"
                 ALL_OTHERS
               else
                 raise "#{self.class.name}: Invalid Build Partial #{ENV['CI_NODE_INDEX']}. At the moment we only support 1 to 9"
               end
  end

  def images?(resource)
    resource.destination_path.start_with?('images/') && !company?(resource)
  end

  def blog_page_new?(resource)
    return true if resource.destination_path =~ %r{sitemaps/sitemap_blog_page_new} # Child sitemap for this partial build

    !proxy_resource?(resource) &&
      ((resource.destination_path.start_with?('blog/') && !blog_page_old?(resource)) ||
        resource.destination_path.end_with?('atom.xml'))
  end

  def blog_page_old?(resource)
    return true if resource.destination_path =~ %r{sitemaps/sitemap_blog_page_old} # Child sitemap for this partial build

    !proxy_resource?(resource) &&
      resource.destination_path.start_with?(
        'blog/2011',
        'blog/2012',
        'blog/2012',
        'blog/2013',
        'blog/2014',
        'blog/2015',
        'blog/2016',
        'blog/2017'
      )
  end

  def release_blog?(resource)
    return true if resource.destination_path =~ %r{sitemaps/sitemap_release_blog} # Child sitemap for this partial build

    !proxy_resource?(resource) &&
      resource.destination_path.start_with?(
        'releases/'
      )
  end

  def handbook?(resource)
    resource.destination_path.start_with?('handbook/')
  end

  def assets?(resource)
    # NOTE that this does NOT include any PDFs, because those end up being handled by middleman in other partials anyway,
    # because they are referred to by proxy resources or `link_to` tags.
    resource.destination_path.start_with?(
      'ico/',
      'stylesheets/',
      'javascripts/'
    )
  end

  def company?(resource)
    return true if resource.destination_path =~ %r{sitemaps/sitemap_company} # Child sitemap for this partial build

    !proxy_resource?(resource) &&
      resource.destination_path.start_with?(
        'images/team/',
        'company/team',
        'company/team-pets',
        'company/culture/all-remote/stories',
        'community/core-team'
      )
  end

  def proxy_resource?(resource)
    # handbook now handles its own proxy resources. It doesn't do remote API
    # calls, so it is not slow. This also is a move towards splitting up responsibility
    # along monorepo sub-site lines. It also means that the `ignore` option on `proxy`
    # works properly in the sub-sites, thus explicit ignores aren't needed in the sub-sites' configs.
    return false if handbook?(resource)

    # These parts of the site are now handled by separate partial build jobs which use DestinationPathRegexesFilter
    return false if assets?(resource)

    return true if resource.destination_path =~ %r{sitemaps/sitemap_proxy_resource} # Child sitemap for this partial build

    # TODO: The rest of these need to eventually be moved to be the responsibility of individual sites
    resource.instance_of?(Middleman::Sitemap::ProxyResource) ||
      resource.destination_path.start_with?('templates/', 'direction/', 'sales/') ||
      resource.destination_path.end_with?('/template.html', 'category.html')
  end

  def all_others?(resource)
    return true if resource.destination_path == 'sitemap.xml' # Include root sitemap index
    return true if resource.destination_path =~ %r{sitemaps/sitemap_all_others} # Include child sitemap for this partial build
    return false if resource.destination_path =~ %r{sitemaps/} # Do not include child sitemaps for any other partial builds

    !proxy_resource?(resource) &&
      !blog_page_new?(resource) &&
      !blog_page_old?(resource) &&
      !release_blog?(resource) &&
      !images?(resource) &&
      !handbook?(resource) &&
      !assets?(resource) &&
      !company?(resource)
  end

  def part_of_partial?(resource)
    case @partial
    when PROXY_RESOURCE
      proxy_resource?(resource)
    when BLOG_POST_OLD
      blog_page_old?(resource)
    when BLOG_POST_NEW
      blog_page_new?(resource)
    when RELEASE_BLOG
      release_blog?(resource)
    when COMPANY
      company?(resource)
    when ALL_OTHERS
      all_others?(resource)
    else
      raise "#{self.class.name}: You are trying to build a unknown partial: #{@partial}"
    end
  end

  def manipulate_resource_list(resources)
    unless @enabled
      logger.info "#{self.class.name}: CI environment variables were not set for a partial build; building everything"
      return resources
    end

    if [UNUSED_1, UNUSED_5, UNUSED_6, UNUSED_7].include?(@partial)
      puts "Skipping build of unused partial job slot #{@partial} from #{self.class.name}; " \
        'these have now been delegated to the other dedicated job(s).'
      # Ensure public directory exists to avoid errors
      FileUtils.mkdir_p(File.expand_path('../public', __dir__))
      return []
    end

    logger.info "#{self.class.name}: We are building the partial: #{@partial}"

    resources.select { |resource| part_of_partial?(resource) }
  end
end

::Middleman::Extensions.register(:partial_build, PartialBuild)
