# NOTE: This is only intended to be used for local development/testing of builds, to call the same commands which
#       the various CI build jobs call.  We don't try to use these rake tasks in CI from the .gitlab-ci.yml,
#       instead we invoke the commands directly, so that build failures have simpler stacktraces
#       that aren't wrapped in the Rake stack.  It's some duplication for now, we may clean
#       it up later as the monorepo work progresses.
namespace :build do
  desc "Build the entire site, including all sub-sites"
  task :all do
    run_cmd('rm -rf public')
    run_cmd('mkdir -p public')

    # Build top level
    build_packaged_assets
    build_assets
    build_marketing_site_in_top_level

    # Build sub-sites
    monorepo_config = YAML.load_file(File.expand_path('../../data/monorepo.yml', __dir__))
    sites = monorepo_config.keys
    sites.delete('blog') # TODO: Skip blog for now because the files are still at the top level, remove this line once the files are actually moved to the marketing sub-site
    sites.each do |site|
      build_site(site)
    end
  end

  desc "Build packaged assets"
  task :packaged_assets do
    build_packaged_assets
  end

  desc "Build assets"
  task :assets do
    build_assets
  end

  # TODO: Temporary, until moved down to /sites/marketing and handled as a monorepo sub-site
  desc "Build marketing site in top level (temporary, until moved down to /sites/marketing and handled as a monorepo sub-site)"
  task :marketing_site_in_top_level do
    build_marketing_site_in_top_level
  end

  desc "Build sites/handbook site"
  task :handbook do
    build_site(:handbook)
  end

  private

  def middleman_build_cmd(env_vars_prefix = '')
    "#{env_vars_prefix} NO_CONTRACTS=#{ENV['NO_CONTRACTS'] || 'true'} middleman build --no-clean --bail"
  end

  def run_cmd(cmd)
    system(cmd) || raise("command failed: #{cmd}")
  end

  def build_packaged_assets
    puts "\n\nBuilding packaged assets..."
    [
      'yarn install',
      'yarn run build',
      'mv tmp/frontend/javascripts public/'
    ].each do |cmd|
      run_cmd(cmd)
    end
  end

  def build_assets
    puts "\n\nBuilding assets..."
    env_vars =
      "DESTINATION_PATH_REGEXES='^ico/,^stylesheets/,^javascripts/' " \
      "MIDDLEMAN_CONFIG_FILE_NAME='config_assets.rb'"
    run_cmd(middleman_build_cmd(env_vars))
  end

  # TODO: Temporary, until moved down to /sites/marketing and handled as a monorepo sub-site
  def build_marketing_site_in_top_level
    puts "\n\nBuilding marketing site in top level..."
    run_cmd(middleman_build_cmd)
  end

  def build_site(site)
    site_dir = File.expand_path("../../sites/#{site}", __dir__)
    Dir.chdir(site_dir) do
      puts "\n\nBuilding '#{site}' site from #{site_dir}..."
      system(middleman_build_cmd) || raise("command failed for '#{site}' site in #{site_dir}: #{middleman_build_cmd}")
    end
  end
end
