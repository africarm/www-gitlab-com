---
layout: handbook-page-toc
title: "GitLab Audit Logging Policy"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GitLab Audit Logging Policy

## Purpose

To ensure the proper operation and security of GitLab.com, GitLab logs critical information system activity. Individual teams and subject matter experts (SMEs) are responsible for determining what constitutes "critical information system activity" in their respective system based on their experience and professional judgement; such activity is then documented either in the handbook or a runbook, whichever is found to be appropriate. Audit logging process is created and implemented by the department(s) or team(s) responsible for a given system.

## Scope

The audit logging policy applies to all systems within our production environment. The production environment includes all endpoints and cloud assets used in hosting GitLab.com and its subdomains. This may include third-party systems that support the business of GitLab.com.

* [What is considered production](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html#what-is-considered-production)
* [Production Architecture](/handbook/engineering/infrastructure/production/architecture/)
* [Audit Logging Matrix](/handbook/engineering/security/guidance/SYS.1.01_audit_logging.html#audit-logging-matrix)