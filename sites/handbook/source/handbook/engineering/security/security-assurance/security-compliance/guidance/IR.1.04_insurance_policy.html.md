---
layout: handbook-page-toc
title: "IR.1.04 - Insurance Policy"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# IR.1.04 - Insurance Policy

## Control Statement

GitLab has insurance policies in place to offset financial impact in the event of loss.

## Context

An insurance policy provides coverage for financial losses that result in many factors including data breaches or cyber events.

## Scope

This control applies to GitLab as a business entity

## Ownership

* Control Owner: `Legal`
* Process owner(s): 
    * Legal: `100%`

## Guidance

The insurance policy documents coverage and applicable monetary limits.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in this [control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/-/issues/1704).

Examples of evidence an auditor might request to satisfy this control:

* Copy of most current insurance policy

### Policy Reference

N/A

## Framework Mapping

* SOC2
  * CC9.1
